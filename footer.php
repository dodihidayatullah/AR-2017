
        <footer>

<div class="m-section m-section-dark">
    <div class="container m-padding-top-70 m-padding-bottom-60">
        <div class="row">
            <div class="row-same-height">
                <div class="col-md-4 col-xs-height col-middle">
                    <div class="m-padding-top-10 m-padding-bottom-10 xs-text-center">
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri().'/assets';?>/images/cifor-white.svg" alt="CIFOR logo">
                        </a>

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri().'/assets';?>/images/cifor-fta-white.svg" alt="CIFOR FTA" style="height:50px;margin-left:15px;">
                        </a>
                    </div>
                </div>

                <div class="col-md-4 col-xs-height col-middle ">
                    <div class="m-width-80 text-center">
                        <a href="" class="btn btn-lg btn-outline-inverse m-uppercase btn-block">
                            Back to top
                        </a>
                    </div>
                </div>

                <div class="col-md-4 col-xs-height col-middle">
                    <div class="text-right m-padding-top-10 xs-text-center">
                        <ul class="social-icons si-circle si-has-color si-small si-outline">
                            <li>
                                <a class="social-icon-google-plus" target="_blank" href="https://plus.google.com/u/0/118290852029073413162?prsrc=6">
                                    <i class="fa fa-google-plus"></i>
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li>
                                <a class="social-icon-facebook" target="_blank" href="http://www.facebook.com/cifor">
                                    <i class="fa fa-facebook"></i>
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a class="social-icon-twitter" target="_blank" href="https://www.twitter.com/cifor">
                                    <i class="fa fa-twitter"></i>
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a class="social-icon-linkedin" target="_blank" href="http://www.linkedin.com/company/center-for-international-forestry-research">
                                    <i class="fa fa-linkedin"></i>
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p class="m-margin-bottom-0 m-margin-top-30">© Center for International Forestry Research 2018</p>
                <p class="m-margin-bottom-0" style="font-size:13px;line-height:1em;margin-top:10px;">
                    This site is licensed under <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) 
                    <span class="cc-license-icons">
                        <span id="cc-logo" class="icon"><img width="15px" alt="cc logo" src="https://creativecommons.org/images/deed/cc_icon_white_x2.png"></span>
                        <span id="cc-attribution" class="icon"><img width="15px"  src="https://creativecommons.org/images/deed/attribution_icon_white_x2.png"></span>
                        <span id="cc-icon-nc" class="icon"><img width="15px"  src="https://creativecommons.org/images/deed/nc_white_x2.png"></span>
                        <span id="cc-icon-sa" class="icon"><img width="15px"  src="https://creativecommons.org/images/deed/sa_white_x2.png"></span>
                    </span>
                    </a></p>
            </div>
        </div>
    </div>
</div>



</footer>
</div>
<?php /*
<script src='vendor/jquery-1.11.1.min.js'></script>
<script src='js/plugins.js'></script>
<script src='vendor/bootstrap-3.3.6/js/bootstrap.min.js'></script>
<script src='js/scripts/init.fmenu.js'></script>
<script src='js/scripts/init.scroll-top.js'></script>
<script src='js/scripts/init.stickynav.js'></script>
<script src='vendor/jquery.sticky.js'></script>
<script src='vendor/venobox/venobox.min.js'></script>
<script src='js/scripts/init.videopopup.js'></script>
<script src='js/scripts/init.fullcover.js'></script>
<script src='js/scripts/init.smoothscroll.js'></script>
<script src='js/scripts/init.car.js'></script>
<script src='vendor/owl-carousel/owl.carousel.min.js'></script>
<script src='js/scripts/init.parallax.js'></script>
<script src='js/scripts/init.right-menu.js'></script>
<script src='vendor/jquery.stellar.js'></script></body>
*/ ?>

<script src='<?php echo get_template_directory_uri().'/assets';?>/vendor/jquery-1.11.1.min.js'></script>
<script src='<?php echo get_template_directory_uri().'/assets';?>/js/plugins.js'></script> 
<!-- <script src='<?php echo get_template_directory_uri().'/assets';?>/vendor/bootstrap-3.3.6/js/bootstrap.min.js'></script>-->
<script src='<?php echo get_template_directory_uri().'/assets';?>/vendor/bootstrap.min.js'></script>
<script src='<?php echo get_template_directory_uri().'/assets';?>/js/scripts/init.fmenu.js'></script>
<!-- 
<script src='<?php echo get_template_directory_uri().'/assets';?>/js/scripts/init.scroll-top.js'></script> -->

<script src='<?php echo get_template_directory_uri().'/assets';?>/js/scripts/init.stickynav.js'></script>
<script src='<?php echo get_template_directory_uri().'/assets';?>/vendor/jquery.sticky.js'></script>
<script src='<?php echo get_template_directory_uri().'/assets';?>/js/scripts/init.fullcover.js'></script>

<!-- 
<script src='<?php echo get_template_directory_uri().'/assets';?>/js/scripts/init.videopopup.js'></script>   
<script src='<?php echo get_template_directory_uri().'/assets';?>/vendor/venobox/venobox.min.js'></script> 
<script src='<?php echo get_template_directory_uri().'/assets';?>/js/scripts/init.smoothscroll.js'></script>

<script src='<?php echo get_template_directory_uri().'/assets';?>/js/scripts/init.car.js'></script>
<script src='<?php echo get_template_directory_uri().'/assets';?>/vendor/owl-carousel/owl.carousel.min.js'></script> -->
<script src='<?php echo get_template_directory_uri().'/assets';?>/js/scripts/init.parallax.js'></script>
<script src='<?php echo get_template_directory_uri().'/assets';?>/js/scripts/init.right-menu.js'></script>
<script src='<?php echo get_template_directory_uri().'/assets';?>/vendor/jquery.stellar.js'></script>
<script src='<?php echo get_template_directory_uri().'/assets';?>/js/linkpop.js'></script>

<?php wp_footer(); ?>



</div>
<!-- End of Wrapper -->



<div class="overlay-menu" id="overlay-menu">
<div class="fmenu-wrapper">
<div class="left-color bg-light m-section-white">
<div class="menu-row">

    <div class="left-menu">
        <ul class="menu list-unstyled m-list-big-margin lead-18">
            <?php 
            $theme_location = 'menu-1';

            if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
                $menu = get_term( $locations[$theme_location], 'nav_menu' );
                $menu_items = wp_get_nav_menu_items($menu->term_id);
            
                foreach( $menu_items as $menu_item ) {
                    if($menu_item->attr_title!='hide_nav'){
                        echo    '<li>
                                    <a href="'.$menu_item->url.'" class="overlay_nav nav-item_'.$menu_item->post_name.'">
                                        '.$menu_item->post_title.'
                                    </a>
                                </li>';
                    }
                }
                
            } else {
                echo '<!-- no menu defined in location "'.$theme_location.'" -->';
            }
            ?>

        </ul>
    </div>

</div>
<div class="fmenu-wrapper-bottom m-padding-bottom-20">
    <div class="menu-row">

        <div class="right-menu">
            <p>
                <small>SHARE</small>
            </p>

            <ul class="social-icons si-has-color si-circle si-colored">
                <li>
                    <a class="social-icon-facebook" href="#">
                        <i class="fa fa-facebook"></i>
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li>
                    <a class="social-icon-linkedin" href="#">
                        <i class="fa fa-linkedin"></i>
                        <i class="fa fa-linkedin"></i>
                    </a>
                </li>
                <li>
                    <a class="social-icon-twitter" href="#">
                        <i class="fa fa-twitter"></i>
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>

                <li>
                    <a class="social-icon-download" href="#">
                        <i class="fa fa-download"></i>
                        <i class="fa fa-download"></i>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</div>
</div>

</div>









</div>

</body>
</html>
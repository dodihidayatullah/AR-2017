<?php /* Template Name: AR-Homepage */ 
get_header(); 
?>

        <!-- Begin Hero section -->
        <a class="anchorjs-link" href="#home">
            <span class="anchorjs-icon"></span>
        </a>

        <?php 
            get_template_part( 'section-parts/section', 'mainslider' );
        ?>
        <!-- End of Hero section -->

        <?php 
        while ( have_posts() ) : the_post();

            $post_id = get_the_ID();

            $section_order = CFS()->get('section_order',$post_id);

            

            foreach($section_order as $pid ){

                // echo "<pre>";
                // print_r($pid);
                // echo"</pre>";

                $s_post = get_post($pid['section_page'][0]);
                $s_data = array('s_post'=>$s_post,'s_title'=>$pid['title'],'s_status'=>$pid['status']);
                $section_style = get_post_meta( $pid['section_page'][0], 'section_style', true );

                set_query_var( 's_data',$s_data);

                if(isset($pid['status'])){
                    if($section_style=='box-left'){
                        get_template_part( 'section-parts/section', 'box-left');
                    }
                    elseif($section_style=='box-right'){
                        get_template_part( 'section-parts/section', 'box-right');
                    }
                    elseif($section_style=='full'){
                        get_template_part( 'section-parts/section', 'full');
                    }
                    elseif($section_style=='default'){
                        get_template_part( 'section-parts/section', 'default');
                    }elseif($section_style=='hero-slide'){

                    }else{
                        get_template_part( 'section-parts/section', 'hero');
                    }
                }
                
                
            }
        endwhile; // End of the loop.
        ?>

<?php get_footer(); ?>
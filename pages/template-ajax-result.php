<?php 
/* Template Name: Ajax Load Result */



// get 
function wp_get_attachment( $attachment_id ) {

	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
    );
    wp_reset_postdata();
}

$js_postid = $_GET['js_postid'];

//if($js_postid){
// echo "<pre>";
// print_r($js_postid);
// echo "</pre>";
$args = array(
	'posts_per_page'   => 1,
	'orderby'          => 'date',
	'order'            => 'DESC',
    'post_type'        => 'page',
    'post__in'         => array($js_postid),
	'post_status'      => 'publish',
    'suppress_filters' => true,
    'no_found_rows'          => true,
    'update_post_term_cache' => false,
    'update_post_meta_cache' => false,
    'cache_results'          => false
);
$posts = get_posts( $args );
foreach ( $posts as $post ) : setup_postdata( $post );  

$parent_id           = wp_get_post_parent_id( get_the_ID() );
$parent_parent_id    = wp_get_post_parent_id( $parent_id );
$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
$caption = wp_get_attachment($post_thumbnail_id);
$caption = $caption['caption'] ? $caption['caption'] : 'Please add caption for this image';

$layout = get_post_meta(get_the_ID(),'fullwidth_layout', true);
if($layout){ $layout_class = "fullwidth"; $layout_side = "hide"; }else{ $layout_class = ""; $layout_side="";}
?>
<article>
    <header class="m-section single-hero-img">
        <div class="top-head-nav">
            <div class="close-btn">
                <div class="close"></div>
            </div>
        </div>
        <div class="pic-caption">
            <div class="content">
                <p><?php echo $caption; ?></p>
            </div>
        </div>
        <?php 
                        
        
        if ( ! $post_thumbnail_id ) {
            echo '<img src="'.get_template_directory_uri().'/assets'.'/images/05b.jpg">';
        }else{
            echo '<img src="'.wp_get_attachment_image_url( $post_thumbnail_id, 'popup_featured' ).'">';
        }
        ?>
    </header>

    <div class="m-section content-single">
        <div class="left-cs cs-container m-section-white <?php echo $layout_class;?>">
            <div class="content-wrapper">
                <div class="entry-header">
                    <div class="breadcrumb">
                        <ul>
                            <li>
                                <?php
								if ( $parent_id ) {
									echo '' . get_the_title( $parent_id ) . '';
								}
								?>
                            </li>
                            <li><?php the_title(); ?></li>
                        </ul>
                    </div>
                    <h1 class="m-margin-bottom-40"><?php the_title(); ?></h1>
                </div>
                <div class="social-share-wrapper text-center">
                    <small>SHARE</small>
                    <ul class="social-icons si-has-color si-circle si-colored">
                        <li>
                            <a class="social-icon-facebook" href="#">
                                <i class="fa fa-facebook"></i>
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a class="social-icon-linkedin" href="#">
                                <i class="fa fa-linkedin"></i>
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a class="social-icon-twitter" href="#">
                                <i class="fa fa-twitter"></i>
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>

                        <li>
                            <a class="social-icon-download" href="#">
                                <i class="fa fa-download"></i>
                                <i class="fa fa-download"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="content-start m-margin-bottom-50">
                    <?php the_content();?>
                </div>
            </div>



            
        </div>
        <div class="right-cs cs-container <?php echo $layout_side; ?>">
            <?php 
            $sidebar = CFS()->get('widgets');
            if($sidebar){
                foreach($sidebar as $widget){

                    //echo $widget['widget_title'];
                    //echo $widget['widget_content'];
                    echo $widget['background_style']['light'];
                    echo $widget['background_style']['dark'];
                    if($widget['background_style']['Light']){
                        $w_class_img = $widget['widget_type']['Image'];
                        if($w_class_img=='image'){ $class="widget-image";}else{$class="";}

                        echo '  <div class="widget '.$class.'">
                                    '.$widget['widget_content'].'
                                </div>
                        ';

                    }
                    if($widget['background_style']['Dark']){
                        echo '  <div class="widget-wrap-dark m-section-dark">
                                    <div class="widget">
                                        '.$widget['widget_content'].'
                                    </div>
                                </div>
                        ';
                    }

                    if($widget['background_style']['Green']){
                        echo '  <div class="widget-wrap-dark m-section-green">
                                    <div class="widget">
                                        '.$widget['widget_content'].'
                                    </div>
                                </div>
                        ';
                    }

                }
            }
            ?>
        </div>
    </div>

    <!-- related link news, video, publication -->
    <div class="m-section m-section-white" style="border-top:1px solid #E5E5E5;">
        <?php 
            $fn_list_title = CFS()->get('forestnews_list_title');
            $fn_list_view_all  = CFS()->get('forestnews_view_all');
            $fn_list  = CFS()->get('forestnews_list');

            if($fn_list){
                $data = array('list_title'=>$fn_list_title,'list_all'=>$fn_list_view_all,'list'=>$fn_list,'style'=>'light','type'=>'blog');
                set_query_var( 'data',$data);
                get_template_part( 'section-parts/section','related-popup');
            } // end if

            $v_list_title = CFS()->get('video_list_title');
            $v_list_view_all  = CFS()->get('video_view_all');
            $v_list  = CFS()->get('video_list');

            if($v_list){
                $data = array('list_title'=>$v_list_title,'list_all'=>$v_list_view_all,'list'=>$v_list,'style'=>'grey','type'=>'video');
                set_query_var( 'data',$data);
                get_template_part( 'section-parts/section','related-popup');
            } // end if 

            $p_list_title = CFS()->get('presentation_list_title');
            $p_list_view_all  = CFS()->get('presentation_view_all');
            $p_list  = CFS()->get('presentation_list');

            if($p_list){
                $data = array('list_title'=>$p_list_title,'list_all'=>$p_list_view_all,'list'=>$p_list,'style'=>'light','type'=>'presentation');
                set_query_var( 'data',$data);
                get_template_part( 'section-parts/section','related-popup');
            } // end if 

            $pub_list_title = CFS()->get('publication_list_title');
            $pub_list_view_all  = CFS()->get('publication_view_all');
            $pub_list  = CFS()->get('publication_list');

            if($pub_list){
                $data = array('list_title'=>$pub_list_title,'list_all'=>$pub_list_view_all,'list'=>$pub_list,'style'=>'grey','type'=>'publication');
                set_query_var( 'data',$data);
                get_template_part( 'section-parts/section','relatedpopup-pub');
            } // end if 
        ?>

        

    </div>
    <!-- Begin Start a related stories -->
    <div class="m-section m-section-split">
        <?php 
        
        $prev_id = get_post_meta(get_the_ID(),'prev_title',true);
        $next_id = get_post_meta(get_the_ID(),'next_title',true);

        ?>
        <div class="m-split-left" style="background: url(<?php echo getThumb($prev_id,'related');?>) no-repeat scroll center center / cover;">
            <div class="content">
                <?php 
                if($prev_id){
                    ?>
                <h6 class="m-uppercase m-letter-spacing-2">Previous Story</h6>
                <h3 class="m-margin-bottom-30">
                    <a class="m-link-white linkpop_pop" href="<?php echo get_the_permalink( $prev_id )?>" data-postid="<?php echo $prev_id; ?>" data-title="<?php echo get_the_title($prev_id); ?>"><?php echo get_the_title($prev_id);?></a>
                </h3>
                <?php 
                }
                ?>
            </div>
            <div class="parallax-overlay split-overlay"></div>
        </div>
        
        <div class="m-split-right" style="background: url(<?php echo getThumb($next_id,'related');?>) no-repeat scroll center center / cover;">

            <div class="content">
                <?php 
                if($next_id){
                ?>
                <h6 class="m-uppercase m-letter-spacing-2">Next Story</h6>
                <h3 class="m-margin-bottom-30">
                    <a class="m-link-white  linkpop_pop" href="<?php echo get_the_permalink( $next_id )?>" data-postid="<?php echo $next_id; ?>" data-title="<?php echo get_the_title($next_id); ?>"><?php echo get_the_title($next_id);?></a>
                </h3>
                <?php 
                }
                ?>
            </div>
            <div class="parallax-overlay split-overlay"></div>
        </div>
        
    </div>
    <!-- End of related stories -->

    <div class="a-close-btn-container m-weight-600">
        <a href="#" class="a-close-btn m-uppercase">
            <span>Close</span> to continue reading <?php echo get_option( 'blogname'); ?>
        </a>

        <div class="close-btn">
            <a href="#" class="close"></a>
        </div>

    </div>
</article>
<?php
endforeach; 
wp_reset_postdata();
?>
<?php 
//}

/*
<div class="right-cs cs-container">
          <div class="widget ">
                                     <h4>Project Info</h4>
                <p>
                    </p><h6><span>Project</span></h6>
                    <h6>Bushmeat Research Initiative</h6>
                <p></p>
                <p>
                    </p><h6><span>Country(ies)</span></h6>
                    <h6>Global</h6>
                <p></p>
                <p>
                    </p><h6><span>Funding partners</span></h6>
                    <h6>USAID, UKAID, DFID</h6>
                <p></p>

                <p>
                    </p><h6><span>CIFOR FOcal point</span></h6>
                    <h6>John E. Fa, Professor at Manchester Metropolitan University and CIFOR Senior Research Associate</h6>
                <p></p>
                                </div>
                          <div class="widget widget-image">
                                    <img src="http://annualreport2017.cifor.org/wp-content/themes/AR-2017/assets/images/side_pic.jpg">
                                </div>
                          <div class="widget ">
                                    <h4>Partnerships for sustainable wild meat</h4>

                <p>
                    Given that hunting wild animals is a global phenomenon and information is sparse, collaboration is critical. CIFOR works
                    with partners in the Bushmeat Research Initiative (BRI), the Collaborative Partnership on Sustainable
                    Wildlife Management (CPW) and the Convention on Biological Diversity (CBD) Liaison Group on Bushmeat.
                    To inform policy, researchers are collating information and sharing it widely through shared databases
                    contained in CIFOR's bushmeat pages (Bushmeat Data Map).
                </p>
                <p>
                    After two years in preparation, alongside three other international partners (FAO, CIRAD and WCS) CIFOR joined the Sustainable
                    Wildlife Management (SWM) consortium focusing on 8 sites in African, Caribbean and Pacific countries.
                    This project, planned for 7 years and receiving 45 M€ aims to reduce hunting of wildlife to sustainable
                    levels, protect endangered wildlife species, conserve biodiversity and maintain essential ecological
                    roles of wildlife within forested and savanna ecosystems. By so doing it will secure stocks and flows
                    of provisioning ecosystems services essential to some of the poorest and most politically marginalized
                    people on the Earth.
                </p>
                                </div>
                          <div class="widget-wrap-dark m-section-dark">
                                    <div class="widget">
                                        <h4>The Guidance in a nutshell</h4>
                    <p>To promote, implement and accelerate integrated action to:
                    </p>
                    <ul>
                        <li>
                            Manage and improve sustainability of wild meat supply at the source

                        </li>
                        <li> Control the excessive demand of wild meat in towns and cities
                        </li>
                        <li>Create enabling conditions for a controlled, sustainable wild meat sector</li>
                    </ul>
                                    </div>
                                </div>
                                </div>
                                */
?>
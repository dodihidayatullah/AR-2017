<?php /* Template Name: Ajax Page */ 
get_header(); 
?>
        <?php 
            get_template_part( 'section-parts/section', 'mainslider' );
        ?>
        <!-- End of Hero section -->

        <?php 
        

            $post_id = 200;

            $section_order = CFS()->get('section_order',$post_id);

            

            foreach($section_order as $pid ){

                // echo "<pre>";
                // print_r($pid);
                // echo"</pre>";

                $s_post = get_post($pid['section_page'][0]);
                $s_data = array('s_post'=>$s_post,'s_title'=>$pid['title'],'s_status'=>$pid['status']);
                $section_style = get_post_meta( $pid['section_page'][0], 'section_style', true );

                set_query_var( 's_data',$s_data);

                if(isset($pid['status'])){
                    if($section_style=='box-left'){
                        get_template_part( 'section-parts/section', 'box-left');
                    }
                    elseif($section_style=='box-right'){
                        get_template_part( 'section-parts/section', 'box-right');
                    }
                    elseif($section_style=='full'){
                        get_template_part( 'section-parts/section', 'full');
                    }
                    elseif($section_style=='default'){
                        get_template_part( 'section-parts/section', 'default');
                    }elseif($section_style=='hero-slide'){

                    }else{
                        get_template_part( 'section-parts/section', 'hero');
                    }
                }
                
                
            }

       
        ?>

<?php get_footer();
 while ( have_posts() ) : the_post();
?>
<script>
jQuery(function($){


    function popupClose(){
        jQuery('.close-btn .close, .a-close-btn, .close-popup').on('click',function(e){
            e.preventDefault();

            jQuery('.single-page.popup').removeClass('active');
            jQuery('.mainfixed-menu').removeClass('hide');
            jQuery('html').removeClass('freeze');
            jQuery('body').removeClass('with-popup fixed');
            
            history.pushState({
                url: js_siteurl
            }, js_blogname, js_siteurl);
            document.title = js_blogname + ' | ' + js_blogdescription;

            return false;
        });
    }
    popupClose();

    function loadnextpost() {
        jQuery('.linkpop_pop').on('click',function (e) {
            e.preventDefault();

            jQuery('.single-page.popup').removeClass('active');
            jQuery('.mainfixed-menu').addClass('hide');
            jQuery('html').removeClass('freeze');
            jQuery('body').removeClass('with-popup fixed');

            var postid = jQuery(this).attr('data-postid');
            var posttitle = jQuery(this).attr('data-title');
            var posturi = jQuery(this).attr("href");

            console.log(postid);
            console.log('next pop');
            
            jQuery('.loading-animate').show();
            
            var js_ajax = js_siteurl+'/ajax/?js_pid='+postid;
            jQuery.ajax({
                url: js_ajax,
                dataType: 'html',
                method: "GET",
                data: { js_postid: postid },
                success: function (result) {
                    // result
                    jQuery('#singleajax').html(result);
                    // change title & url
                    history.pushState({
                        url: posturi+"?js_pid="+postid
                    }, js_blogdescription, posturi+"?js_pid="+postid);
                    document.title = posttitle + ' | ' + js_blogdescription;
                    // dom action
                    jQuery('.mainfixed-menu').addClass('hide');
                    jQuery('.single-page.popup').addClass('active');
                    jQuery('html').addClass('freeze');
                    jQuery('body').addClass('with-popup fixed');
                },
                complete: function () {
                    popupClose();
                    loadnextpost();
                    jQuery('.loading-animate').hide();
                },
                error: function (jqXHR, textStatus) {
                    //alert("Request failed: " + textStatus);
                }
            });
        });
    }
    


    jQuery('.single-page.popup').removeClass('active');
    jQuery('.mainfixed-menu').removeClass('hide');
    jQuery('html').removeClass('freeze');
    jQuery('body').removeClass('with-popup fixed');

    var postid = js_pid;
    var posttitle = js_ptitle;
    var posturi = js_purl;

    jQuery('.loading-animate').show();

    var js_ajax = js_siteurl+'/ajax/?js_pid='+postid;
    jQuery.ajax({
        url: js_ajax,
        dataType: 'html',
        method: "GET",
        data: { js_postid: postid },
        success: function (result) {
            // result
            jQuery('#singleajax').html(result);
            // change title & url
            history.pushState({
                url: posturi+"?js_pid="+postid
            }, js_blogdescription, posturi+"?js_pid="+postid);
            document.title = posttitle + ' | ' + js_blogdescription;
            // dom action
            jQuery('.mainfixed-menu').addClass('hide');
            jQuery('.single-page.popup').addClass('active');
            jQuery('html').addClass('freeze');
            jQuery('body').addClass('with-popup fixed');

            jQuery('.loading-animate').hide();
            
        },
        complete: function () {
            popupClose();
            loadnextpost();
            jQuery('.loading-animate').hide();
        },
        error: function (jqXHR, textStatus) {
            //alert("Request failed: " + textStatus);
        }
    });

});
</script>

<?php 
endwhile; // End of the loop.
?>
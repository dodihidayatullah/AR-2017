<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <!-- Basic page needs -->
    <meta charset="utf-8">
    <!-- Libs CSS -->
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/css/animate.css'>
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/css/normalize.css'>
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/vendor/bootstrap-3.3.6/css/bootstrap.min.css'>
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/vendor/owl-carousel/owl.carousel.css'>
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/vendor/venobox/venobox.css'>
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/vendor/pe-icon-7-stroke/css/pe-icon-7-stroke.css'>

    <!-- Theme CSS -->
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/css/style.css'>
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/css/custom.css'>
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/css/fmenu.css'>
    
    <?php 
    wp_head();
    ?>

    <!-- Mobile meta tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Default theme -->
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/css/color.css'>

    <!-- Default fontawesome -->
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/vendor/font-awesome/css/font-awesome.min.css'>

    <!-- Responsive stylesheet -->
    <link rel='stylesheet' href='<?php echo get_template_directory_uri().'/assets';?>/css/responsive.css'>

    <!-- Web fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,900,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic' rel='stylesheet' type='text/css'>

    <!-- Head Libs -->
    <script src='<?php echo get_template_directory_uri().'/assets';?>/vendor/modernizr-2.8.1.min.js'></script>
</head>

<!-- Add your site or application content here -->

<body <?php body_class(); ?>> <!-- 'with-popup fixed' -->
    <div class="loading-animate">
        <div class="la-ball-scale-pulse la-3x">
            <div></div>
            <div></div>
        </div>
    </div>
    <?php 
        get_template_part( 'section-parts/single', 'ajax-popup');
    ?>
    <!-- Begin Wrapper -->
    <div id="wrapper">
        <!-- Begin sticky menu section -->
        <header class="m-section-base menu-inline-2 mainfixed-menu">
            <div id="sticknav" class="sticky-navigation navbar-collapse nav-main-collapse collapse">
                <div class="menu-container">
                    <div class="fmenu-btn toggle" id="fmenu">
                        <div class="button-container">
                            <span class="top"></span>
                            <span class="middle"></span>
                            <span class="bottom"></span>
                            <span class="very-bottom"></span>
                        </div>

                        <div class="button-logo-container">

                        </div>

                        <div class="nav-title bs-docs-sidebar">
                            <ul class="panels-section nav bs-docs-sidenav">
                                <?php 
                                $theme_location = 'menu-1';

                                if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
                                    $menu = get_term( $locations[$theme_location], 'nav_menu' );
                                    $menu_items = wp_get_nav_menu_items($menu->term_id);
                                
                                    foreach( $menu_items as $menu_item ) {
                                
                                        echo    '<li>
                                                    <a href="'.$menu_item->url.'" class="nav-item_'.$menu_item->post_name.'">
                                                        '.$menu_item->post_title.'
                                                    </a>
                                                </li>';
                                    }
                                    
                                } else {
                                    echo '<!-- no menu defined in location "'.$theme_location.'" -->';
                                }
                                ?>
                                <?php /*
                                <li>
                                    <a href="#dg-letter" class="main-scroll-nav">
                                        DG Letter
                                    </a>
                                </li>
                                <li>
                                    <a href="#best-of" class="main-scroll-nav">
                                        Best of 2017
                                    </a>
                                </li>
                                <li>
                                    <a href="#influencing" class="main-scroll-nav">
                                        Influencing Policies and Governments
                                    </a>
                                </li>
                                <li>
                                    <a href="#research-for-impact" class="main-scroll-nav">
                                        Research for Impact
                                    </a>
                                </li>
                                <li>
                                    <a href="#capacity-building" class="main-scroll-nav">
                                        Capacity Building
                                    </a>
                                </li>
                                <li>
                                    <a href="#outreach-engagement" class="main-scroll-nav">
                                        Outreach and Engagement
                                    </a>
                                </li>
                                <li>
                                    <a href="#gender" class="main-scroll-nav">
                                        Gender
                                    </a>
                                </li>

                                <li>
                                    <a href="#cifor-numbers" class="main-scroll-nav">
                                        CIFOR by Numbers
                                    </a>
                                </li>

                                <li>
                                    <a href="#where-we-work" class="main-scroll-nav">
                                        Where We Work
                                    </a>
                                </li>

                                <li>
                                    <a href="#partners-finances" class="main-scroll-nav">
                                        Partners & Finances
                                    </a>
                                </li <li>
                                <a href="#about" class="main-scroll-nav">
                                    About CIFOR
                                </a>
                                </li> */ ?>

                        </div>

                        <div class="button-logo-container">
                            <a href="">
                                <!--
                                <img src="http://annualreport2017.cifor.org/wp-content/uploads/2018/03/CIFOR-25th-logo-white@x1.png">-->
                                
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/cifor-logo.svg">
                            </a>
                        </div>

                        
                    </div>
                    <div class="download-pdf-link-wrapper">
                        <a href="#" class="m-link-white m-uppercase"><img src="<?php echo get_template_directory_uri();?>/assets/images/share-icon.svg"> </a>
                        <a href="#" class="m-link-white m-uppercase"><img src="<?php echo get_template_directory_uri();?>/assets/images/download-icon.svg"> </a>
                    </div>
                </div>

            </div>
        </header>

        <!-- End of sticky menu section -->
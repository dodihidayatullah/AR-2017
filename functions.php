<?php
/**
 * AR-2017 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package AR-2017
 */

if ( ! function_exists( 'annual_report_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function annual_report_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on AR-2017, use a find and replace
		 * to change 'annual-report' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'annual-report', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'annual-report' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'annual_report_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'annual_report_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function annual_report_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'annual_report_content_width', 640 );
}
add_action( 'after_setup_theme', 'annual_report_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function annual_report_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'annual-report' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'annual-report' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'annual_report_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function annual_report_scripts() {
	wp_enqueue_style( 'annual-report-style', get_stylesheet_uri() );

	//wp_enqueue_script( 'annual-report-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	//wp_enqueue_script( 'annual-report-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );


	//wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '20170301', true );
	//wp_enqueue_script( 'bootstrap-propper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array(), '20170301', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'annual_report_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/************************************************************************************************* */

$main_uri = 'http://datasources.cifor.org'; // prod

function cu_getJsonFile($uri,$page=null,$row=null){
	global $main_uri; 
	$param_uri  = $uri;
	$json_uri = $main_uri.$param_uri;

	$args = array('timeout'=>1200);

	$json_get = wp_remote_get( $json_uri, $args);
	
	//$json_result = json_encode($json_get);
	if( is_wp_error(  $json_get ) ) {
		$json_get = $response->get_error_message();
		return "Something went wrong: $error_message";
	} else {
		$data = json_decode($json_get['body']);

		$page = $data->current_page;
		$page_total = $data->total_pages;
		$total_result = $data->total_result;
		$show_perpage = $data->limit_per_page;
		$records = $data->records;

		return array(
			'page'=>$page,
			'page_total'=>$page_total,
			'total_result'=>$total_result,
			'show_perpage'=>$show_perpage,
			'records'=>$records
		);
	}
}

function cu_getJsonFile_no($uri,$page=null,$row=null){
    global $main_uri; 
    $param_uri  = $uri;
    $json_uri = $main_uri.$param_uri;
	if(strpos($param_uri, "category/country")) { $json_uri = $json_uri . "&total=0";}
    $args = array('timeout'=>120);
    $json_get = wp_remote_get( $json_uri, $args);
    //$json_result = json_encode($json_get);
    if( is_wp_error(  $json_get ) ) {
        $json_get = $response->get_error_message();
        return "Something went wrong: $error_message";
    } else {
        $data = json_decode($json_get['body']);
        return $data;
    }
}

function arHead(){
	echo '
		<script>
			var js_blogname 		=	"'.get_option('blogname').'"; 
			var js_blogdescription	=   "'.get_option('blogdescription').'";
			var js_siteurl			= 	"'.get_option('siteurl').'";
			var js_pid				=   "'.get_the_ID().'";
			var js_ptitle			=   "'.get_the_title().'";
			var js_purl				=   "'.get_the_permalink().'";
		</script>
	';
}
add_action( 'wp_head', 'arHead', 10);



// [bartag foo="foo-value"]
// function bartag_func( $atts ) {
//     $a = shortcode_atts( array(
//         'foo' => 'something',
//         'bar' => 'something else',
//     ), $atts );

//     return print_r($atts);//"foo = {$a['foo']}";
// }
// add_shortcode( 'bartag', 'bartag_func' );

if ( ! function_exists( 'short_sidebox_shortcode' ) ) {
	function short_sidebox_shortcode( $atts, $content = null ) {

		global $cfs;
		global $post;
		$attr = CFS()->get( 'side_box', $post->ID );

		$a         = shortcode_atts( array( 'id' => '' ), $atts );
		$get_value = filter_by_value( $attr, 'side_id', $a['id'] );

		foreach ( $get_value as $key ) {
			$side_content   = $key['side_content'];
			$side_title   = $key['side_title'];
			$side_fullwidth   = $key['side_fullwidth'];
		}

		if($side_fullwidth){ $class="box-content"; }else{ $class="related-content"; }

		$content = '
					<aside class="'.$class.'">
						<h5 class="m-uppercase related-c-title">'.$side_title.'</h5>
						'.$side_content.'
					</aside>';
		
		// Code
		return $content;
	
	}

	add_shortcode( 'sbox', 'short_sidebox_shortcode' );
}


if ( ! function_exists( 'short_quote_shortcode' ) ) {
	function short_quote_shortcode( $atts, $content = null ) {

		global $cfs;
		global $post;
		$attr = CFS()->get( 'quote_box', $post->ID );

		$a         = shortcode_atts( array( 'id' => '' ), $atts );
		$get_value = filter_by_value( $attr, 'quote_id', $a['id'] );

		foreach ( $get_value as $key ) {
			$quote_desc   = $key['quote_desc'];
			$quote_author = $key['quote_author'];
			$quote_from   = $key['quote_from'];
			$quote_source = $key['quote_source'];
		}

		$short_link = '
					<ul class="testimonials testimonials-has-border list-unstyled one-col">
                        <li class="item">
                            <div class="testimonial-item">
                                <div class="testimonial-entry m-margin-bottom-20 lead-20 m-font-brandon-grotesque-light">
									'. $quote_desc .'
                                </div>
                                <h6 class="m-margin-bottom-0">'. $quote_author .'</h6>
                                <p class="m-margin-bottom-0 m-opacity-70">'.$quote_from.'</p>
                            </div>
                        </li>
					</ul>';
		
		// Code
		return $short_link;
	}

	add_shortcode( 'squote', 'short_quote_shortcode' );
}

if ( ! function_exists( 'short_html_shortcode' ) ) {
	function short_html_shortcode( $atts, $content = null ) {
		global $cfs;
		global $post;
		$attr = CFS()->get( 'html_shortcode', $post->ID );
		// Attributes
		$a         = shortcode_atts( array( 'id' => 'id' ), $atts );
		$get_value = filter_by_value( $attr, 'html_id', $a['id'] );
		foreach ( $get_value as $key ) {
			$html_code   = $key['html_code'];
			$custome_css = $key['custome_css'];
		}
		$short_link = '<div id="bot-list">' . $html_code . '</div><style>' . $custome_css . '</style>';

		// Code
		return $short_link;
	}

	add_shortcode( 'shtml', 'short_html_shortcode' );
}
/*
 * filtering an array
 */
if ( ! function_exists( 'filter_by_value' ) ) {
	function filter_by_value( $array, $index, $value ) {
		if ( is_array( $array ) && count( $array ) > 0 ) {
			foreach ( array_keys( $array ) as $key ) {
				$temp[ $key ] = $array[ $key ][ $index ];
				if ( $temp[ $key ] == $value ) {
					$newarray[ $key ] = $array[ $key ];
				}
			}
		}

		return $newarray;
	}
}
// shortcode dropcap
function dropcap( $atts, $content = null ) {
	return '<span class="dropcap">' . $content . '</span>';
}

add_shortcode( 'dropcap', 'dropcap' );
// shortcode lead style
function ar_lead( $atts, $content = null ) {
	return '<p class="lead-28 m-font-museo-100 m-text-green m-opacity-50">' . $content . '</p>';
}

add_shortcode( 'lead', 'ar_lead' );
// shortcode list style
function ar_list( $atts, $content = null ) {
	return str_replace( '<ul>', '<ul class="list-unstyled m-list-solid">', $content );
}

add_shortcode( 'slist', 'ar_list' );
function ar_row( $atts, $content = null ) {
	$a = shortcode_atts( array( 'class' => 'class' ), $atts );

	return '<div class="row ' . $a['class'] . '">' . do_shortcode( $content ) . '</div>';
}

add_shortcode( 'srow', 'ar_row' );
function ar_col6( $atts, $content = null ) {
	return '<div class="col-md-6 col-sm-6"><article>' . $content . '</article></div>';
}

add_shortcode( 'scol6', 'ar_col6' );
function getLastPathSegment( $url ) {
	$path        = parse_url( $url, PHP_URL_PATH ); // to get the path from a whole URL
	$pathTrimmed = trim( $path, '/' ); // normalise with no leading or trailing slash
	$pathTokens  = explode( '/', $pathTrimmed ); // get segments delimited by a slash
	if ( substr( $path, - 1 ) !== '/' ) {
		array_pop( $pathTokens );
	}

	return end( $pathTokens ); // get the last segment
}


/*
* get thumbnail
*/
function getThumb($pid,$size){
	$size = $size ? 'related' : 'medium';
	$post_thumbnail_id = get_post_thumbnail_id( $pid );
	if ( ! $post_thumbnail_id ){
		//$post_thumbnail_id = get_post_thumbnail_id( 250 );
		$image = wp_get_attachment_image_url( 250, $size );
	}else{
		$image = wp_get_attachment_image_url( $post_thumbnail_id, $size );
	}
	return $image;
}


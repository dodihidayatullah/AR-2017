<!-- Begin Hero section -->
<a class="anchorjs-link" href="#home">
    <span class="anchorjs-icon"></span>
</a>

<div id="main-slider" class="m-section full-cover" style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/bg_cover.jpg);">
    <div class="home-container">
        <div class="home-slide-text miombo-header">
            <div class="text-wrapper">
                <div class="">
                    <h2 class="m-uppercase web-cat m-margin-bottom-30">ANNUAL REPORT 2017</h2>
                    <h2 class="m-font-brandon-grotesque-medium h1 m-text-white">Building sustainable landscapes,
                        <br> one policy at a time</h2>
                </div>
            </div>

            <div class="home-bottom">
                <div class="container text-center">
                    <div class="move">
                        <a href="#a-about">
                            <div class="mouse">
                                <span></span>
                            </div>
                            <i class="fa fa-angle-down"></i>
                        </a>

                    </div>
                </div>
            </div>

            <div class="home-links">
                <div class="text-center">
                    <ul class="list-unstyled home-link-items">
                        <li>
                            <a href="">
                                Influencing Policies and Governments
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Forestry Research for Impact
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Forestry Research for Impact
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Outreach and Engagement
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Gender
                            </a>
                        </li>
                        <li>
                            <a href="">
                                CIFOR by the Numbers
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Where we work
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- End of Hero section -->

<!-- Begin DG section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="dg-letter" class="m-margin-bottom-0">
        </h2>
    </div>


    <div class="theme-hero-container m-section m-section-parallax" style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/dg.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title-wrap">
                        <div class="section-title">
                            <h2 class="h1"> Letters endrerit quasi sequi veniam</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-section m-section-light-green">
        <div class="container m-padding-bottom-80">
            <div class="row row-eq-height">
                <div class="col-md-6">
                    <div class="section-blurb m-padding-top-40 m-padding-bottom-50">
                        <a href="" class="btn btn-outline-inverse btn-lg btn-block m-uppercase" href="#">
                            Letter from the DG
                        </a>
                        <a href="" class="btn btn-outline-inverse btn-lg btn-block m-uppercase" href="#">
                            Letter from the board chair
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="lead-18 m-padding-top-40">Pretium saepe feugiat ab nascetur amet consequatur odio, diamlorem malesuada. Consequat adipiscing, vitae
                        ornare! Blandit, congue cubilia hac aut felis ipsam iaculis tortor malesuada odit tempor iste pretium,
                        laudantium aute curabitur malesuada tempus ligula! Adipiscing tellus quasi. Tempor dolores, aut.</p>


                </div>
            </div>
        </div>
    </div>

</div>
<!-- End of DG section -->

<!-- Begin Best of section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="best-of" class="m-margin-bottom-0">
        </h2>
    </div>


    <div class="theme-hero-container m-section m-section-parallax" data-stellar-vertical-offset="500" data-stellar-background-ratio="0.8"
        style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/best-of.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title-wrap">
                        <div class="section-title">
                            <h2 class="h1"> CIFOR best of 2017</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-section m-section-light-green">
        <div class="container m-padding-bottom-80">
            <div class="row row-eq-height">

                <div class="col-md-6">
                    <div class="section-blurb m-padding-top-40 m-padding-bottom-50">
                        <p class="lead-22 m-font-brandon-text-medium">The year 2017 was a time for action. With global targets in place under the Sustainable Development
                            Goals and the Paris Agreement, CIFOR prioritized research in six thematic areas.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="lead-18 m-padding-top-40">Pretium saepe feugiat ab nascetur amet consequatur odio, diamlorem malesuada. Consequat adipiscing, vitae
                        ornare! Blandit, congue cubilia hac aut felis ipsam iaculis tortor malesuada odit tempor iste pretium,
                        laudantium aute curabitur malesuada tempus ligula! Adipiscing tellus quasi. Tempor dolores, aut.</p>
                    <a href="" class="btn btn-outline btn-dark m-uppercase">Learn More</a>


                </div>
            </div>
        </div>
    </div>

</div>
<!-- End of Best of section -->

<!-- Begin Influencing section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="influencing" class="m-margin-bottom-0">
        </h2>
    </div>


    <div class="theme-hero-container m-section m-section-parallax" data-stellar-vertical-offset="100" data-stellar-background-ratio="0.4"
        style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/influencing.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title-wrap">
                        <div class="section-title">
                            <h2 class="h1"> Influencing Policies and Governments</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-section m-section-light-green">
        <div class="container m-padding-bottom-80">
            <div class="row row-eq-height">
                <div class="col-md-6">
                    <div class="section-blurb m-padding-top-40 m-padding-bottom-50">
                        <p class="lead-22 m-font-brandon-text-medium">Eos torquent, aliquet atque magni tristique. At maecenas interdum eiusmod pariatur turpis euismod
                            aliquid, distinctio, ac, ultrices unde.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="lead-18 m-padding-top-40">Pretium saepe feugiat ab nascetur amet consequatur odio, diamlorem malesuada. Consequat adipiscing, vitae
                        ornare! Blandit, congue cubilia hac aut felis ipsam iaculis tortor malesuada odit tempor iste pretium,
                        laudantium aute curabitur malesuada tempus ligula! Adipiscing tellus quasi. Tempor dolores, aut.</p>


                </div>
            </div>
        </div>
        <div class="container m-padding-bottom-80">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ar-flexbox">
                        <div class="ar-col-f">
                            <a href="">
                                <img src="<?php echo get_template_directory_uri().'/assets';?>/images/influencing-thumb/blog_med1.jpg"> </a>
                            <div class="flexb-title-wrap">
                                <div id="triangle-up"></div>
                                <h3>
                                    <a class="m-link-dark" href="">Vitae doloribus provident repudiandae! Dis egestas</a>
                                </h3>
                            </div>
                        </div>
                        <div class="ar-col-f">
                            <a href="">
                                <img src="<?php echo get_template_directory_uri().'/assets';?>/images/influencing-thumb/blog_med2.jpg"> </a>
                            <div class="flexb-title-wrap">
                                <div id="triangle-up"></div>
                                <h3>
                                    <a class="m-link-dark" href="">Turpis, cupiditate reiciendis condimentum, cubilia massa</a>
                                </h3>
                            </div>
                        </div>
                        <div class="ar-col-f">
                            <a href="">
                                <img src="<?php echo get_template_directory_uri().'/assets';?>/images/influencing-thumb/blog_med3.jpg"> </a>
                            <div class="flexb-title-wrap">
                                <div id="triangle-up"></div>
                                <h3>
                                    <a class="m-link-dark" href="">Vivamus donec, lectus optio, optio amet sagittis sekum</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="ar-flexbox">
                        <div class="ar-col-f">
                            <a href="">
                                <img src="<?php echo get_template_directory_uri().'/assets';?>/images/influencing-thumb/blog_med4.jpg"> </a>
                            <div class="flexb-title-wrap">
                                <div id="triangle-up"></div>
                                <h3>
                                    <a class="m-link-dark" href="">Eos parturient eum egestas nemo magni nemo fames, aliquip phasellus</a>
                                </h3>
                            </div>
                        </div>
                        <div class="ar-col-f">
                            <a href="">
                                <img src="<?php echo get_template_directory_uri().'/assets';?>/images/influencing-thumb/blog_med5.jpg"> </a>
                            <div class="flexb-title-wrap">
                                <div id="triangle-up"></div>
                                <h3>
                                    <a class="m-link-dark" href="">Urna nam molestias, eget dicta fames aliquet</a>
                                </h3>
                            </div>
                        </div>
                        <div class="ar-col-f">
                            <a href="">
                                <img src="<?php echo get_template_directory_uri().'/assets';?>/images/influencing-thumb/blog_med6.jpg"> </a>
                            <div class="flexb-title-wrap">
                                <div id="triangle-up"></div>
                                <h3>
                                    <a class="m-link-dark" href="">Beatae faucibus aliquip eget scelerisque rutrum laboris</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- End of Influencing section -->

<!-- Begin Research for Impact section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="research-for-impact" class="m-margin-bottom-0">
        </h2>
    </div>


    <div class="m-section pilar-wrapper m-section-white">
        <div class="container-fluid">
            <div class="row row-eq-height pillar-nm">
                <div class="col-md-8 col-md-push-4">
                    <div class="pillar-content m-padding-top-40 m-padding-bottom-40">
                        <h2 class="h1 m-margin-bottom-30">Research for Impact</h2>
                        <p>Natoque potenti metus varius ab leo, ad, aliqua condimentum, elit. Iusto placeat inventore tellus
                            montes sunt, anim sollicitudin dictum quaerat etiam do, ullamcorper metus repudiandae eros tristique
                            molestiae incididunt ipsa. Urna laborum. Diamlorem, laboris occaecati porta, justo doloremque
                            tortor et.</p>

                        <p>Ducimus quas neque justo maecenas aut laudantium illo tempora, cupiditate, laboris, proin felis in
                            soluta, reiciendis. Beatae nisl corporis tenetur aptent? Taciti? Voluptate facilisi. Saepe ea
                            dicta libero! Phasellus distinctio ipsum quaerat quidem auctor alias! Perferendis id sodales
                            excepteur bibendum.</p>
                    </div>
                </div>
                <div class="col-md-4 col-md-pull-8">
                    <div class="pillar-infographics m-padding-top-40 m-padding-bottom-40">
                        <h2>[Infographics will be placed here.]</h2>
                    </div>

                </div>
            </div>

        </div>

        <div class="infographic-wrapper">

        </div>

        <div class="pilar-info-wrapper">

        </div>
    </div>

</div>
<!-- End of Research for Impact section -->

<!-- Begin Capacity building section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="capacity-building" class="m-margin-bottom-0">
        </h2>
    </div>


    <div class="m-section pilar-wrapper m-section-light-blue">
        <div class="container-fluid">
            <div class="row row-eq-height pillar-nm">
                <div class="col-md-8">
                    <div class="pillar-content m-padding-top-40 m-padding-bottom-40">
                        <h2 class="h1 m-margin-bottom-30">Research for Impact</h2>
                        <p>Natoque potenti metus varius ab leo, ad, aliqua condimentum, elit. Iusto placeat inventore tellus
                            montes sunt, anim sollicitudin dictum quaerat etiam do, ullamcorper metus repudiandae eros tristique
                            molestiae incididunt ipsa. Urna laborum. Diamlorem, laboris occaecati porta, justo doloremque
                            tortor et.</p>

                        <p>Ducimus quas neque justo maecenas aut laudantium illo tempora, cupiditate, laboris, proin felis in
                            soluta, reiciendis. Beatae nisl corporis tenetur aptent? Taciti? Voluptate facilisi. Saepe ea
                            dicta libero! Phasellus distinctio ipsum quaerat quidem auctor alias! Perferendis id sodales
                            excepteur bibendum.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pillar-infographics m-padding-top-40 m-padding-bottom-40">
                        <h2>[Infographics will be placed here.]</h2>
                    </div>

                </div>
            </div>

        </div>

        <div class="infographic-wrapper">

        </div>

        <div class="pilar-info-wrapper">

        </div>
    </div>

</div>
<!-- End of Capacity building section -->

<!-- Begin Outreach engagement section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="outreach-engagement" class="m-margin-bottom-0">
        </h2>
    </div>


    <div class="m-section pilar-wrapper m-section-white">
        <div class="container-fluid">
            <div class="row row-eq-height pillar-nm">
                <div class="col-md-8 col-md-push-4">
                    <div class="pillar-content m-padding-top-40 m-padding-bottom-40">
                        <h2 class="h1 m-margin-bottom-30">Research for Impact</h2>
                        <p>Natoque potenti metus varius ab leo, ad, aliqua condimentum, elit. Iusto placeat inventore tellus
                            montes sunt, anim sollicitudin dictum quaerat etiam do, ullamcorper metus repudiandae eros tristique
                            molestiae incididunt ipsa. Urna laborum. Diamlorem, laboris occaecati porta, justo doloremque
                            tortor et.</p>

                        <p>Ducimus quas neque justo maecenas aut laudantium illo tempora, cupiditate, laboris, proin felis in
                            soluta, reiciendis. Beatae nisl corporis tenetur aptent? Taciti? Voluptate facilisi. Saepe ea
                            dicta libero! Phasellus distinctio ipsum quaerat quidem auctor alias! Perferendis id sodales
                            excepteur bibendum.</p>
                    </div>
                </div>
                <div class="col-md-4 col-md-pull-8">
                    <div class="pillar-infographics m-padding-top-40 m-padding-bottom-40">
                        <h2>[Infographics will be placed here.]</h2>
                    </div>

                </div>
            </div>

        </div>

        <div class="infographic-wrapper">

        </div>

        <div class="pilar-info-wrapper">

        </div>
    </div>

</div>
<!-- End of Outreach engagement section -->

<!-- Begin Gender section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="gender" class="m-margin-bottom-0">
        </h2>
    </div>

    <div class="theme-hero-container m-section m-section-parallax" data-stellar-vertical-offset="120" data-stellar-background-ratio="0.4"
        style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/gender_parallax.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title-wrap">
                        <div class="section-title">
                            <h2 class="h1"> Gender across CIFOR’s work</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-section m-section-light-green">
        <div class="container m-padding-bottom-80">
            <div class="row row-eq-height">

                <div class="col-md-6">
                    <div class="section-blurb m-padding-top-40 m-padding-bottom-50">
                        <ul class="testimonials testimonials-has-border list-unstyled one-col">
                            <li class="item">
                                <div class="testimonial-item">
                                    <div class="testimonial-entry m-margin-bottom-20 lead-20 m-font-brandon-grotesque-light">
                                        Tenetur ipsum? Aliquam primis libero distinctio, doloremque pellentesque, pede eveniet dignissimos euismod, platea? Elementum
                                        fuga! Curae amet adipisicing varius ornare consequuntur enim, sint irure aliquid.

                                    </div>

                                    <div class="testimonial-pic m-icon-small m-icon-small-left">
                                        <img src="<?php echo get_template_directory_uri().'/assets';?>/images/faces/face_4.jpg">
                                    </div>
                                    <h6 class="m-margin-bottom-0">Richard Doe</h6>

                                    <p class="m-margin-bottom-0 m-opacity-70">Fermentum Architecto</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="lead-18 m-padding-top-40">CIFOR takes a rights-based approach to gender equality. Beyond the simple recognition that failing to
                        understand local-level gender dynamics can skew research findings, we ground our work in the idea
                        that all humans deserve an equal opportunity to thrive. Understanding gender dynamics is both a focus
                        of specific research projects and a key aspect in all of CIFOR’s activities.

                    </p>
                    <a href="" class="btn btn-outline btn-dark m-uppercase">Learn More</a>


                </div>
            </div>
        </div>
    </div>

</div>
<!-- End of Gender section -->

<!-- Begin CIFOR by numbers section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="cifor-numbers" class="m-margin-bottom-0">
        </h2>
    </div>


    <div class="theme-hero-container m-section m-section-parallax" data-stellar-vertical-offset="120" data-stellar-background-ratio="0.4"
        style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/gender_parallax.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title-wrap">
                        <div class="section-title">
                            <h2 class="h1">CIFOR by Numbers</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-section m-section-light-green">
        <div class="container m-padding-bottom-80">
            <div class="row row-eq-height">

                <div class="col-md-6">
                    <div class="section-blurb m-padding-top-40 m-padding-bottom-50">
                        <p class="lead-22 m-font-brandon-text-medium">The year 2017 was a time for action. With global targets in place under the Sustainable Development
                            Goals and the Paris Agreement, CIFOR prioritized research in six thematic areas.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="lead-18 m-padding-top-40">CIFOR takes a rights-based approach to gender equality. Beyond the simple recognition that failing to
                        understand local-level gender dynamics can skew research findings, we ground our work in the idea
                        that all humans deserve an equal opportunity to thrive. Understanding gender dynamics is both a focus
                        of specific research projects and a key aspect in all of CIFOR’s activities.

                    </p>



                </div>
            </div>
        </div>
        <div class="container m-padding-bottom-80">
            <div class="row">
                <div class="col-sm-12">
                    <h2>[Infographics will be placed here.]</h2>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- End of CIFOR by numbers section -->

<!-- Begin Where we work section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="where-we-work" class="m-margin-bottom-0">
        </h2>
    </div>



    <div class="m-section m-section-light-blue">
        <div class="container m-padding-bottom-80 m-padding-top-70">
            <div class="row">

                <div class="col-md-4">
                    <h2 class="h1">Where We Work</h2>
                </div>

                <div class="col-md-8">
                    <p class="lead-18">Eos torquent, aliquet atque magni tristique. At maecenas interdum eiusmod pariatur turpis euismod aliquid,
                        distinctio, ac, ultrices unde euismod iure aliquid quasi cupidatat mi, fugiat? Rutrum penatibus dui.
                        Metus nec vehicula dictumst </p>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- End of Where we work section -->

<!-- Begin Partners section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="partners-finances" class="m-margin-bottom-0">
        </h2>
    </div>


    <div class="theme-hero-container m-section m-section-parallax" data-stellar-vertical-offset="120" data-stellar-background-ratio="0.4"
        style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/gender_parallax.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title-wrap">
                        <div class="section-title">
                            <h2 class="h1"> Partners, Board of Trustees & Finance</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-section m-section-light-green">
        <div class="container m-padding-bottom-80">
            <div class="row row-eq-height">

                <div class="col-md-6">
                    <div class="section-blurb m-padding-top-40 m-padding-bottom-50">
                        <a href="" class="btn btn-outline-inverse btn-lg btn-block m-uppercase" href="#">
                            Partners
                        </a>
                        <a href="" class="btn btn-outline-inverse btn-lg btn-block m-uppercase" href="#">
                            Board of Trustees
                        </a>
                        <a href="" class="btn btn-outline-inverse btn-lg btn-block m-uppercase" href="#">
                            Finance
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="lead-18 m-padding-top-40">CIFOR’s work is possible thanks to the financial support of our Funding Partners and the collaboration
                        of our Strategic Partners. We work closely with a range of local and international organizations
                        and institutions to deliver research projects with the greatest potential impact.

                    </p>


                </div>
            </div>
        </div>
    </div>

</div>
<!-- End of Partners section -->

<!-- Begin About section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="about" class="m-margin-bottom-0">
        </h2>
    </div>


    <div class="theme-hero-container m-section m-section-parallax" style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/bg_cifor.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title-wrap">
                        <div class="section-title">
                            <h2 class="h1"> About CIFOR</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-section m-section-light-green">
        <div class="container m-padding-bottom-80">
            <div class="row row-eq-height">
                <div class="col-md-6">
                    <div class="section-blurb m-padding-top-40 m-padding-bottom-50">

                    </div>
                </div>
                <div class="col-md-6">
                    <p class="lead-18 m-padding-top-40">CIFOR advances human well-being, equity and environmental integrity by conducting innovative research,
                        developing partners’ capacity, and actively engaging in dialogue with all stakeholders to inform
                        policies and practices that affect forests and people. CIFOR is a CGIAR Research Center, and leads
                        the CGIAR Research Program on Forests, Trees and Agroforestry (FTA). Our headquarters are in Bogor,
                        Indonesia, with offices in Nairobi, Kenya, Yaounde, Cameroon, and Lima, Peru.
                    </p>
                    <a href="" class="btn btn-outline btn-dark m-uppercase">More on CIFOR & The CGIAR</a>

                </div>
            </div>
        </div>
    </div>

</div>
<!-- End of About section -->
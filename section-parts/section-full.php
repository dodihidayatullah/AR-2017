<?php 
/*  
* section-parts/section-full.php
*/ 
$s_ID                   = $s_data['s_post']->ID;
$parallax               = get_post_meta( $s_ID, 'parallax',true );
$subtitle_description   = get_post_meta( $s_ID, 'section_description', true);
$css_id                 = get_post_meta( $s_ID, 'css_id', true);
$css_class              = get_post_meta( $s_ID, 'css_class', true);
$show_relatedpost       = get_post_meta( $s_ID, 'show_related_post', true);
$relatedpost            = get_post_meta( $s_ID, 'related_post', true);
$button                 = CFS()->get('button',$s_ID);
$html_code              = get_post_meta($s_ID, 'html_code_section', true);
?>
<!-- Begin Where we work section -->
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="id_<?php echo sanitize_title($s_data['s_title']); ?>" class="m-margin-bottom-0">
        </h2>
    </div>



    <div class="m-section m-section-light-blue">
        <div class="container m-padding-bottom-20 m-padding-top-70">
            <div class="row">

                <div class="col-md-4">
                    <h2 class="h1"><?php echo $s_data['s_title'];?></h2>
                </div>

                <div class="col-md-8">
                    <div class="lead-18">
                        <?php 
                            if($s_data['s_post']->post_content!=''){
                                $content_post = get_post($s_ID);
                                $content = $content_post->post_content;
                                $content = apply_filters('the_content', $content);
                                $content = str_replace(']]>', ']]&gt;', $content);
                                echo $content;
                                //echo get_post_field('post_content', $s_ID);
                            }else{
                                echo  'Pretium saepe feugiat ab nascetur amet consequatur odio, diamlorem malesuada. Consequat adipiscing, vitae
                                ornare! Blandit, congue cubilia hac aut felis ipsam iaculis tortor malesuada odit tempor iste pretium,
                                laudantium aute curabitur malesuada tempus ligula! Adipiscing tellus quasi. Tempor dolores, aut.';
                            }

                            echo '</p><p>';
                            // show button left
                            if(isset($button)){
                                if(count($button) > 0){
                                    echo '<div class="button-box">';
                                    foreach($button as $btn ){
                                        if(isset($btn['position']['right'])){

                                            if($btn['link']['target']=='_blank'){
                                                $target = 'target="_blank"';
                                            }else{
                                                $target = '';
                                            }
                                            echo '<a href="'.$btn['link']['url'].'" '.$target.' class="btn btn-outline btn-dark m-uppercase"> '.$btn['link']['text'].' </a>';

                                        }
                                    }
                                    echo '</div>';
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container m-padding-bottom-80 ">
            <div class="row">
                <?php 
                    if($html_code){
                        echo $html_code;
                    }
                ?>
            </div>
        </div>
    </div>
</div>
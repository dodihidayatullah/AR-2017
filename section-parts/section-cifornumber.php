<div id="infographic-wrapper">

    <div class="">
        <p style="margin-top: 20px;margin-bottom: 20px">CIFOR’s contribution to the global policy dialogue gained more international recognition this year.</p>
        <div class="row">

            <div class="col-xs-12 col-sm-6 ">
                <div class="">
                    <div class="row">
                        <div class="col-xs-12">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="ig-trophy-cell">
                                            <img src="http://ar2016.azureedge.net/wp-content/themes/wp-annual-report-2016/assets/images/infographics/trophy-1.png"
                                                height="65" scale="0">
                                        </td>
                                        <td style="padding-left: 10px;vertical-align:top;">
                                            <span class="ig-big-text" style="font-size: 15px;font-weight: bold;">out of 100 top Climate Think Tanks</span>
                                            <br>
                                            <p style="font-size: 0.9em;">International Center for Climate Governance</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 ">
                <div class="">
                    <div class="row">
                        <div class="col-xs-12">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="ig-trophy-cell">
                                            <img src="http://ar2016.azureedge.net/wp-content/themes/wp-annual-report-2016/assets/images/infographics/trophy-2.png"
                                                height="65" scale="0">
                                        </td>
                                        <td style="padding-left: 10px;vertical-align:top;">
                                            <span class="ig-big-text" style="font-size: 15px;font-weight: bold;">out of 95 top Environment Policy Think Tanks</span>
                                            <br>
                                            <p style="font-size: 0.9em;line-height:16px;">Think Tanks and Civil Societies Program: Global Go To Think Tank Index Report</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <p style="margin-top: 20px;">CIFOR and its partners contribute to the following global processes, frameworks, panels and conventions:</p>
    <div>
        <div class="row">
            <div class="col-xs-12">
                <img src="http://ar2016.azureedge.net/wp-content/themes/wp-annual-report-2016/assets/images/Logo.png" style="margin-bottom: 50px;"
                    scale="0">
            </div>
        </div>
    </div>

    <p>CIFOR launched a set of key performance indicators in 2016 to chart our impact through research, capacity development
        and engagement, and to measure our operational performance.</p>
    <div>
        <div class="row">
            <div class="col-xs-12">
                <img src="http://ar2016.azureedge.net/wp-content/themes/wp-annual-report-2016/assets/images/infographics/KPI-WEB-infographics.jpg"
                    style="margin-bottom: 30px;" scale="0">
            </div>
        </div>
    </div>


    <div class="theme-feature-container threecol with-highlight m-margin-bottom-30">
        <div class="highlights-box tf-box" style="background-image: url(http://ar2016.azureedge.net/wp-content/uploads/2017/04/top-banner-non-research-meia-460x175.png);background-size: cover">
            <a href="http://annualreport2016.cifor.org/cifor-strategy/monitoring-evaluation-and-impact-assessment/" class="fs-overlay-container linkpop"
                data-title="Monitoring, evaluation and impact assessment" data-postid="2619" data-uri="http://annualreport2016.cifor.org/cifor-strategy/monitoring-evaluation-and-impact-assessment/">
                <div class="fs-overlay">
                    <h3> Monitoring, evaluation and impact assessment </h3>
                </div>
            </a>
        </div>
        <div class="highlights-box tf-box" style="background-image: url(http://ar2016.azureedge.net/wp-content/uploads/2017/03/coe-thumbnail.png);background-size: cover">
            <a href="http://annualreport2016.cifor.org/cifor-strategy/communication-outreach-and-engagement/" class="fs-overlay-container linkpop"
                data-title="Communication, outreach and engagement" data-postid="1451" data-uri="http://annualreport2016.cifor.org/cifor-strategy/communication-outreach-and-engagement/">
                <div class="fs-overlay">
                    <h3> Communication, outreach and engagement </h3>
                </div>
            </a>
        </div>
        <div class="highlights-box tf-box" style="background-image: url(http://ar2016.azureedge.net/wp-content/uploads/2017/03/glf-thumbnail.png);background-size: cover">
            <a href="http://annualreport2016.cifor.org/cifor-strategy/global-landscapes-forum/" class="fs-overlay-container linkpop"
                data-title="Global Landscapes Forum" data-postid="1728" data-uri="http://annualreport2016.cifor.org/cifor-strategy/global-landscapes-forum/">
                <div class="fs-overlay">
                    <h3> Global Landscapes Forum </h3>
                </div>
            </a>
        </div>
    </div>

</div>
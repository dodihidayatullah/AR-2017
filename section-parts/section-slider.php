<div id="main-slider" class="m-section full-cover" style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/bg_cover.jpg);">
    <div class="home-container">
        <div class="home-slide-text miombo-header">
            <div class="text-wrapper">
                <div class="">
                    <h2 class="m-uppercase web-cat m-margin-bottom-30">ANNUAL REPORT 2017</h2>
                    <h2 class="m-font-brandon-grotesque-medium h1 m-text-white">Building sustainable landscapes,
                        <br> one policy at a time</h2>
                </div>
            </div>

            <div class="home-bottom">
                <div class="container text-center">
                    <div class="move">
                        <a href="#a-about">
                            <div class="mouse">
                                <span></span>
                            </div>
                            <i class="fa fa-angle-down"></i>
                        </a>

                    </div>
                </div>
            </div>

            <div class="home-links">
                <div class="text-center">
                    <ul class="list-unstyled home-link-items">
                        <li>
                            <a href="">
                                Influencing Policies and Governments
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Forestry Research for Impact
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Forestry Research for Impact
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Outreach and Engagement
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Gender
                            </a>
                        </li>
                        <li>
                            <a href="">
                                CIFOR by the Numbers
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Where we work
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


</div>
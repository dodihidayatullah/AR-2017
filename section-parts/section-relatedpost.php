<?php 
$rp_data = $rp_data;

$args = array(
	'posts_per_page'   => 5,
	'orderby'          => 'date',
	'order'            => 'DESC',
    'post_type'        => 'page',
    'post__in'         => $rp_data,
	'post_status'      => 'publish',
    'suppress_filters' => true,
    'no_found_rows'          => true,
    'update_post_term_cache' => false,
    'update_post_meta_cache' => false,
    'cache_results'          => false
);
$posts = get_posts( $args );
// echo "<pre>";
// print_r($posts);
// echo"</pre>";
?>
<style>
.thumb-wrapper{
    max-height:250px;
    overflow:hidden;
}
.thumb-wrapper img{
    width:100%;
}
</style>
<div class="container m-padding-bottom-80">
    <div class="row">
        <div class="col-sm-12">
            <?php
            //Columns must be a factor of 12 (1,2,3,4,6,12)
            $numOfCols = 2;
            $rowCount = 0;
            $bootstrapColWidth = 12 / $numOfCols;
            ?>
            <div class="ar-flexbox">
            <?php
            foreach ( $posts as $post ) : setup_postdata( $post );
            ?>  
                <div class="ar-col-f relatedpost-box">
                    <div class="thumb-wrapper">
                        <a href="<?php the_permalink(); ?>" class="linkpop" data-postid="<?php echo get_the_ID();?>"  data-title="<?php the_title();?>">
                        <?php 
                        
                        $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
                        if ( ! $post_thumbnail_id ) {
                            echo '<img src="'.get_template_directory_uri().'/assets/images/influencing-thumb/blog_med1.jpg">';
                        }else{
                            echo '<img src="'.wp_get_attachment_image_url( $post_thumbnail_id, 'full' ).'">';
                        }

                        ?>
                        </a>
                    </div>
                    <div class="flexb-title-wrap">
                        <div id="triangle-up"></div>
                        <h3><a class="m-link-dark linkpop" href="<?php the_permalink();?>" data-postid="<?php echo get_the_ID();?>"  data-title="<?php the_title();?>"><?php the_title();?></a></h3>
                    </div>
                </div>
                <?php /*
                    <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                        <div class="thumbnail">
                            <img src="user_file/<?php echo $row->foto; ?>">
                        </div>
                    </div> */ ?>
            <?php
                $rowCount++;
                if($rowCount % $numOfCols == 0) echo '</div><div class="ar-flexbox">';
            endforeach; 
            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>
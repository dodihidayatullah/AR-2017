<?php 
$rp_data = $rp_data;
$args = array(
    'posts_per_page'   => 5,
    'orderby' => 'post__in',
	//'orderby'          => 'date',
	//'order'            => 'DESC',
    'post_type'        => 'page',
    'post__in'         => $rp_data,
	'post_status'      => 'publish',
    'suppress_filters' => true,
    'no_found_rows'          => true,
    'update_post_term_cache' => false,
    'update_post_meta_cache' => false,
    'cache_results'          => false
);
$posts = get_posts( $args );
?>

<hr class="m-divider-margin-60">
<!-- Begin Main second level links section -->
<?php 
if($rp_data_id==98){ ?>
<div class="ar-flexbox single-pillar two-links">
    <div class="ar-col-f" style="margin: 3px 0 0px 0;">
      <?php 
            $post_thumbnail_id = get_post_thumbnail_id( 100 );
            if ( ! $post_thumbnail_id ) {
                $image = get_template_directory_uri().'/assets/images/bg-box.jpg';
            }else{
                $image = 'https://annualreport2017.cifor.org/wp-content/uploads/2018/04/glf.jpg';
            }
        ?>
        <a class="flexb-title-wrap m-link-white linkpop"  href="https://annualreport2017.cifor.org/outreach-and-engagement/glf-peatlands-bonn/" data-postid="100" data-title="<?php echo get_the_title(100);?>" 
        style="background: url(<?php echo $image; ?>) no-repeat scroll center center / cover;">
           <div class="flex-container">
               <div class="link-icon icon-glf">
                   <!-- <img src="<?php echo get_template_directory_uri();?>/assets/images/filter_none.svg"> -->
                   <img src="<?php echo get_template_directory_uri();?>/assets/images/logo_glf.png">
               </div>
               <div class="link-content title-glf">
                   <h3 class="m-margin-bottom-0"> <?php echo get_the_title(100);?></h3>
               </div>
           </div>
            <!-- <div class="parallax-overlay split-overlay"></div> -->
        </a>
    </div>
</div>
<div class="ar-flexbox single-pillar two-links ">
    <div class="ar-col-f">
        <?php 
            $post_thumbnail_id = get_post_thumbnail_id( 104 );
            if ( ! $post_thumbnail_id ) {
                $image = get_template_directory_uri().'/assets/images/bg-box.jpg';
            }else{
                $image = wp_get_attachment_image_url( $post_thumbnail_id, 'medium' );
            }
        ?>
        <a class="flexb-title-wrap m-link-white linkpop" href="https://annualreport2017.cifor.org/outreach-and-engagement/coe-by-the-numbers/" data-postid="104" data-title="<?php echo get_the_title(104);?>" 
        style="background: url(<?php echo $image; ?>) no-repeat scroll center center / cover;">
           <div class="flex-container">
               <!-- <div class="link-icon">
                   <img src="<?php echo get_template_directory_uri();?>/assets/images/filter_none.svg">
               </div> -->
               <div class="link-content">
                   <h3 class="m-margin-bottom-0"><?php echo get_the_title(104);?></h3>
               </div>
           </div>
            <!-- <div class="parallax-overlay split-overlay"></div> -->
        </a>
    </div>
    <div class="ar-col-f">
        <?php 
            $post_thumbnail_id = get_post_thumbnail_id( 102 );
            if ( ! $post_thumbnail_id ) {
                $image = get_template_directory_uri().'/assets/images/bg-box.jpg';
            }else{
                $image = wp_get_attachment_image_url( $post_thumbnail_id, 'medium' );
            }
        ?>
        <a class="flexb-title-wrap m-link-white linkpop" href="https://annualreport2017.cifor.org/outreach-and-engagement/events-partnerships/" data-postid="102" data-title="<?php echo get_the_title(102);?>"  
        style="background: url(<?php echo $image; ?>) no-repeat scroll center center / cover;">
            <div class="flex-container">
                <!-- <div class="link-icon">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/filter_none.svg">
                </div> -->
                <div class="link-content">
                    <h3 class="m-margin-bottom-0"> <?php echo get_the_title(102);?> </h3>
                </div>
            </div>
            <!-- <div class="parallax-overlay split-overlay"></div> -->
        </a>
    </div>
</div> 
<?php
}else{
 ?>
<!-- End of Main second level links section -->
<!-- Begin Second level links section -->
<div class="ar-flexbox single-pillar">
<?php
        foreach ( $posts as $post ) : setup_postdata( $post );

        $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
        if ( ! $post_thumbnail_id ) {
            $image = get_template_directory_uri().'/assets/images/bg-box.jpg';
        }else{
            $image = wp_get_attachment_image_url( $post_thumbnail_id, 'medium' );
        }
        ?> 
        <div class="ar-col-f" style="background: url(<?php echo $image; ?>) no-repeat scroll center center / cover;">
        <a class="flexb-title-wrap m-link-white linkpop bglink" 
            href="<?php the_permalink();?>" 
            data-postid="<?php echo get_the_ID();?>" 
            data-title="<?php the_title();?>"
             >
            
            <h6 class="m-text-white"><?php echo get_post_meta(get_the_ID(),'subtitle',true);?></h6>
            <h4><?php the_title();?> </h4>
        </a>
        </div>
        <?php
        endforeach; 
        wp_reset_postdata();
        ?>
</div>
<?php 
}
?>
<!-- End of Second level links section -->


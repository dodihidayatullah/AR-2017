<?php 
$z_data = count($data['list']);
?>
<hr class="m-divider-margin-10">
<div class="content-wrapper-bottom">
    <div class="m-padding-top-10 m-padding-bottom-10 clearfix">
        <h3 class="m-uppercase pull-left"><?php echo $data['list_title'];  ?></h3>
        <h4 class="pull-right">
            <a href="<?php echo $data['list_all']['url']; ?>" class="m-link-dark">
                <?php echo $data['list_all']['text']; ?>
            </a>
        </h4>
    </div>
</div>
<div class="m-section m-section-grey m-padding-top-30 m-padding-bottom-30 <?php echo 'section-'.$data['type'];?>">
    <div class="content-wrapper-bottom">
        <div class="ar-flexbox single-related <?php if($z_data == 1){ echo 'single-item'; }?>">
            <?php 
            
            foreach($data['list'] as $n){
                if($n['dace_source']){

                    $uri = '/api/dace2/resource/type/list/?_format=json&id='.$n['dace_id'].'&contenttype='.$data['type'];
                    $api = cu_getJsonFile($uri,1,3);

                    $title  = !empty($n['title']) ? $n['title'] : $api['records'][0]->dcTitle;
                    $link   = $api['records'][0]->dcDocumentUri;
                    $image  = $api['records'][0]->dcDocumentThumbnail;
                    $class  = "";
                    if($data['type']=='publication'){
                        $image = 'http://www.cifor.org/uploads/webservice/'.$image;
                        $class = 'thumb-publication';
                        
                    }
                    if($n['image']){
                        if($z_data==1){
                            $image = wp_get_attachment_image_src($n['image'],'full');
                        }else{
                            $image = wp_get_attachment_image_src($n['image'],'medium');
                        }
                        $image = $image[0];
                    }
                    
                    echo '
                    <div class="ar-col-f">
                        <a href="'.$link.'" class="link-image">
                            <div class="image-wrap_popup '.$class.' image-'.$data['type'].'">
                                <img src="'.$image.'"> 
                            </div>
                        </a>
                        <div class="flexb-title-wrap">
                            <h4>
                                <a class="m-link-dark" href="'.$link.'">'.$title.'</a>
                            </h4>
                        </div>
                    </div>';


                }else{

                    if($n['image']){
                        if($z_data==1){
                            $image = wp_get_attachment_image_src($n['image'],'full');
                        }else{
                            $image = wp_get_attachment_image_src($n['image'],'medium');
                        }
                        $image = $image[0];
                    }else{
                        
                        if($data['type']=='publication'){
                            $image = 'https://annualreport2017.cifor.org/wp-content/uploads/2018/03/thumb.jpg';
                        }else{
                            $image = get_template_directory_uri().'/assets/images/influencing-thumb/blog_med1.jpg';
                        }
                    }
                    
                    echo '
                    <div class="ar-col-f">
                        <a href="'.$n['link']['url'].'" class="link-image">
                            <div class="image-wrap_popup '.$class.' image-'.$data['type'].'">
                                <img src="'.$image.'"> 
                            </div>
                        </a>
                        <div class="flexb-title-wrap">
                            <h4>
                                <a class="m-link-dark" href="'.$n['link']['url'].'">'.$n['title'].'</a>
                            </h4>
                        </div>
                    </div>';


                }
            }
            ?>

        </div>
    </div>
</div>

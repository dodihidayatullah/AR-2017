<?php 
/*  
* section-parts/section-box-right.php
*/ 

$s_ID                   = $s_data['s_post']->ID;
$parallax               = get_post_meta( $s_ID, 'parallax',true );
$subtitle_description   = get_post_meta( $s_ID, 'section_description', true);
$css_id                 = get_post_meta( $s_ID, 'css_id', true);
$css_class              = get_post_meta( $s_ID, 'css_class', true);
$show_relatedpost       = get_post_meta( $s_ID, 'show_related_post', true);
$relatedpost            = get_post_meta( $s_ID, 'related_post', true);
$button                 = CFS()->get('button',$s_ID);
$html_code              = get_post_meta($s_ID, 'html_code_section', true);
?>

<div class="m-section">
    <div class="nav-section-title">
        <h2 id="capacity-building" class="m-margin-bottom-0"></h2>
    </div>


    <div class="m-section pilar-wrapper m-section-light-blue">
        <div class="container-fluid">
            <div class="row row-eq-height pillar-nm">
                <div class="col-md-8">
                    <div class="pillar-content m-padding-top-40 m-padding-bottom-40">
                        <h2 class="h1 m-margin-bottom-30"><?php echo $s_data['s_title'];?></h2>
                        <?php 
                            if($s_data['s_post']->post_content!=''){
                                $content_post = get_post($s_ID);
                                $content = $content_post->post_content;
                                $content = apply_filters('the_content', $content);
                                $content = str_replace(']]>', ']]&gt;', $content);
                                echo $content;
                                //echo get_post_field('post_content', $s_ID);
                            }else{
                                echo  'Pretium saepe feugiat ab nascetur amet consequatur odio, diamlorem malesuada. Consequat adipiscing, vitae
                                ornare! Blandit, congue cubilia hac aut felis ipsam iaculis tortor malesuada odit tempor iste pretium,
                                laudantium aute curabitur malesuada tempus ligula! Adipiscing tellus quasi. Tempor dolores, aut.';
                            }

                            echo '</p><p>';

                            if(isset($show_relatedpost)){
                                if($show_relatedpost){
                                    $rp_data = CFS()->get('related_post',$s_ID);
                                    set_query_var( 'rp_data',$rp_data);
                                    get_template_part( 'section-parts/section', 'more-box' );
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pillar-infographics m-padding-top-40 m-padding-bottom-40">
                        <?php 
                        if($html_code){
                            echo $html_code;
                        }
                        ?>
                    </div>

                </div>
            </div>

        </div>

        <div class="infographic-wrapper">

        </div>

        <div class="pilar-info-wrapper">

        </div>
    </div>

</div>
<?php 
$s_ID                   = $s_data['s_post']->ID;
$parallax               = get_post_meta( $s_ID, 'parallax',true );
$subtitle_description   = get_post_meta( $s_ID, 'section_description', true);
$css_id                 = get_post_meta( $s_ID, 'css_id', true);
$css_class              = get_post_meta( $s_ID, 'css_class', true);
$show_relatedpost       = get_post_meta( $s_ID, 'show_related_post', true);
$relatedpost            = get_post_meta( $s_ID, 'related_post', true);
$button                 = CFS()->get('button',$s_ID);
$html_code              = get_post_meta($s_ID, 'html_code_section', true);

//echo "<pre>";
//print_r($s_data);
//print_r($parallax);
//print_r($button);
//echo"</pre>";
?>
<div class="m-section">
    <div class="nav-section-title">
        <h2 id="id_<?php echo sanitize_title($s_data['s_title']); ?>" class="m-margin-bottom-0"></h2>
    </div>
    <div id="box_<?php echo sanitize_title($s_data['s_title']); ?>" class="theme-hero-container m-section m-section-parallax" 
    
    <?php 
    
    if(isset($parallax)){
        if($parallax==1){
            echo 'data-stellar-vertical-offset="0" data-stellar-background-ratio="0.05"';
        }
    }
    
    ?>
    data-id="<?php echo $s_ID;?>" style="background-image: url(<?php
    
        $post_thumbnail_id = get_post_thumbnail_id( $s_ID );
        if ( ! $post_thumbnail_id ) {
            echo get_template_directory_uri().'/assets/images/dg.jpg';
        }else{
            echo wp_get_attachment_image_url( $post_thumbnail_id, 'full' );
        }
        
        ?>
        );">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title-wrap">
                        <div class="section-title">
                            <h2 class="h1"><?php echo $s_data['s_title'];?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-section m-section-light-green">
        <div class="container <?php if($html_code){ echo "m-padding-bottom-20"; }else{  echo "m-padding-bottom-80";  }?>">
            <div class="row row-eq-height">
                <div class="col-md-6">
                    <div class="section-blurb m-padding-top-40 m-padding-bottom-50">
                        <?php 
                        // show cf subtitle description
                        if(isset($subtitle_description)){
                            if(!empty($subtitle_description)){
                                echo ''.$subtitle_description.'';
                            }
                        }
                        echo "<p></p>";
                        // show button left
                        if(isset($button)){
                            if(count($button) > 0){
                                foreach($button as $btn ){
                                    if(isset($btn['position']['left'])){

                                        if($btn['link']['target']=='_blank'){
                                            $target = 'target="_blank"';
                                        }else{
                                            $target = '';
                                        }

                                        if($btn['page_link_status']){
                                            $page_link_id = $btn['page_link'][0];
                                            echo '<a href="'.get_the_permalink($page_link_id).'"  data-postid="'.$page_link_id.'" data-title="'.get_the_title($page_link_id).'" class="linkpop btn btn-outline-inverse btn-lg btn-block m-uppercase"> '.$btn['title'].' </a>';
                                        }else{
                                            echo '<a href="'.$btn['link']['url'].'" '.$target.' class="linkpop btn btn-outline-inverse btn-lg btn-block m-uppercase"> '.$btn['link']['text'].' </a>';
                                        }

                                    }
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="lead-18 m-padding-top-40">
                        <?php 

                            if($s_data['s_post']->post_content!=''){
                                $content_post = get_post($s_ID);
                                $content = $content_post->post_content;
                                $content = apply_filters('the_content', $content);
                                $content = str_replace(']]>', ']]&gt;', $content);
                                echo $content;
                            }else{
                                if($s_ID==108){
                                    echo '<p style="margin-top: 20px;margin-bottom: 20px">CIFOR’s contribution to the global policy dialogue gained more international recognition this year.</p>
                                   
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="col-sm-2">
                                                            <img src="https://annualreport2017.cifor.org/wp-content/themes/AR-2017/assets/images/ig/top-3.svg" height="65" scale="0">
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <p style="font-size:15px;line-height:1.3em;"><strong>out of 100 top Climate Think Tanks</strong><br> International Center for Climate Governance</p>
                                                        </div>
                                                                        
                                                                    
                                                                    
                                                                  
                                                    </div>
                                                </div>
                                                <p></p>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="col-sm-2">
                                                            <img src="https://annualreport2017.cifor.org/wp-content/themes/AR-2017/assets/images/ig/top-5.svg" height="65" scale="0">
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <p style="font-size:15px;line-height:1.3em;">Two articles by CIFOR scientists were
                                                            among the top 5 most influential articles
                                                            of 2016 published in Environmental
                                                            Evidence, and were in the top 5% of all
                                                            research outputs ever tracked by Altmetric</p>
                                                        </div>      
                                                    </div>
                                                </div>
                                           
                            
                                    </div>';
                                }else{
                                    echo $s_ID;
                                    echo  'Please add description for this section';
                                }
                                
                            }

                            echo '</p><p>';
                            // show button left
                            if(isset($button)){
                                if(count($button) > 0){
                                    echo '<div class="button-box">';
                                    foreach($button as $btn ){
                                        if(isset($btn['position']['right'])){

                                            if($btn['link']['target']=='_blank'){
                                                $target = 'target="_blank"';
                                            }else{
                                                $target = '';
                                            }
                                            // echo "<pre>";
                                            // print_r($btn);
                                            // echo "</pre>";

                                            if($btn['page_link_status']){
                                                $page_link_id = $btn['page_link'][0];
                                                echo '<a href="'.get_the_permalink($page_link_id).'"  data-postid="'.$page_link_id.'" data-title="'.get_the_title($page_link_id).'" class="linkpop btn btn-outline btn-dark m-uppercase"> '.$btn['title'].' </a>';
                                            }else{
                                                echo '<a href="'.$btn['link']['url'].'" '.$target.' class="linkpop btn btn-outline btn-dark m-uppercase"> '.$btn['link']['text'].' </a>';
                                            }
                                        }
                                    }
                                    echo '</div>';
                                }
                            }
                        ?>
                    
                    </div>
                </div>
            </div>
        </div>

        <?php 
        if(isset($show_relatedpost)){
            if($show_relatedpost){

                $rp_data = CFS()->get('related_post',$s_ID);
                $rp_data_start = array_shift($rp_data);
                //echo "<pre>";
                //print_r($rp_data);
                //echo "</pre>";

                set_query_var( 'rp_data',$rp_data);
                get_template_part( 'section-parts/section', 'relatedpost');
            }
        }
        ?>
        <?php 
        if($html_code){
        ?>
        <div class="m-section m-section-light-green">
            <div class="container m-padding-bottom-80 ">
                <div class="row">
                    <?php 
                        
                            echo $html_code;
                        
                    ?>
                </div>
            </div>
        </div>
        <?php 
        }
        ?>

    </div>
</div>
<div id="main-slider" class="m-section full-cover" style="background-image: url(<?php echo get_template_directory_uri().'/assets';?>/images/bg_cover.jpg);">
            <div class="home-container">
                <div class="home-slide-text miombo-header">
                    <div class="text-wrapper">
                        <div class="">
                            <h2 class="m-uppercase web-cat m-margin-bottom-30">ANNUAL REPORT 2017</h2>
                            <h2 class="m-font-brandon-grotesque-medium h1 m-text-white">Building sustainable landscapes,
                                <br> one policy at a time</h2>
                        </div>
                    </div>

                    <div class="home-bottom">
                        <div class="container text-center">
                            <div class="move">
                                <a href="#a-about">
                                    <div class="mouse">
                                        <span></span>
                                    </div>
                                    <i class="fa fa-angle-down"></i>
                                </a>

                            </div>
                        </div>
                    </div>

                    <div class="home-links">
                        <div class="text-center">
                            <ul class="list-unstyled home-link-items">
                                <?php 
                                $theme_location = 'menu-1';

                                if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
                                    $menu = get_term( $locations[$theme_location], 'nav_menu' );
                                    $menu_items = wp_get_nav_menu_items($menu->term_id);
                                
                                    foreach( $menu_items as $menu_item ) {
                                        if($menu_item->attr_title!='hide_nav'){
                                        echo    '<li>
                                                    <a href="'.$menu_item->url.'" class="nav-item_'.$menu_item->post_name.'">
                                                        '.$menu_item->post_title.'
                                                    </a>
                                                </li>';
                                        }
                                    }
                                    
                                } else {
                                    echo '<!-- no menu defined in location "'.$theme_location.'" -->';
                                }
                                ?>
                                <!-- <li>
                                    <a href="">
                                        Influencing Policies and Governments
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        Research for Impact
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        Capacity Development
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        Outreach and Engagement
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        Gender Across CIFOR's Work
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        CIFOR by the Numbers
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        About CIFOR
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


        </div>
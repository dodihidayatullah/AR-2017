jQuery(function($){

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    
    function popupClose(){
        jQuery('.close-btn .close, .a-close-btn, .close-popup').on('click',function(e){
            e.preventDefault();

            jQuery('.single-page.popup').removeClass('active');
            jQuery('.mainfixed-menu').removeClass('hide');
            jQuery('html').removeClass('freeze');
            jQuery('body').removeClass('with-popup fixed');
            
            history.pushState({
                url: js_siteurl
            }, js_blogname, js_siteurl);
            document.title = js_blogname + ' | ' + js_blogdescription;

            return false;
        });
    }
    popupClose();

    jQuery('.linkpop').click(function (e) {
        e.preventDefault();

        jQuery('.single-page.popup').removeClass('active');
        jQuery('.mainfixed-menu').addClass('hide');
        jQuery('html').removeClass('freeze');
        jQuery('body').removeClass('with-popup fixed');

        var postid = jQuery(this).attr('data-postid');
        var posttitle = jQuery(this).attr('data-title');
        var posturi = jQuery(this).attr("href");
        
        jQuery('.loading-animate').show();

        var js_ajax = js_siteurl+'/ajax/?js_pid='+postid;
        jQuery.ajax({
            url: js_ajax,
            dataType: 'html',
            method: "GET",
            data: { js_postid: postid },
            success: function (result) {
                // result
                jQuery('#singleajax').html(result);
                // change title & url
                history.pushState({
                    url: posturi+"?js_pid="+postid
                }, js_blogdescription, posturi+"?js_pid="+postid);
                document.title = posttitle + ' | ' + js_blogdescription;
                // dom action
                jQuery('.mainfixed-menu').addClass('hide');
                jQuery('.single-page.popup').addClass('active');
                jQuery('html').addClass('freeze');
                jQuery('body').addClass('with-popup fixed');
            },
            complete: function () {
                popupClose();
                loadnextpost();
                jQuery('.loading-animate').hide();
            },
            error: function (jqXHR, textStatus) {
                //alert("Request failed: " + textStatus);
            }
        });
    });

    function loadnextpost() {
        jQuery('.linkpop_pop').on('click',function (e) {
            e.preventDefault();

            jQuery('.single-page.popup').removeClass('active');
            jQuery('.mainfixed-menu').addClass('hide');
            jQuery('html').removeClass('freeze');
            jQuery('body').removeClass('with-popup fixed');

            var postid = jQuery(this).attr('data-postid');
            var posttitle = jQuery(this).attr('data-title');
            var posturi = jQuery(this).attr("href");

            console.log(postid);
            console.log('next pop');
            
            jQuery('.loading-animate').show();
            
            var js_ajax = js_siteurl+'/ajax/?js_pid='+postid;
            jQuery.ajax({
                url: js_ajax,
                dataType: 'html',
                method: "GET",
                data: { js_postid: postid },
                success: function (result) {
                    // result
                    jQuery('#singleajax').html(result);
                    // change title & url
                    history.pushState({
                        url: posturi+"?js_pid="+postid
                    }, js_blogdescription, posturi+"?js_pid="+postid);
                    document.title = posttitle + ' | ' + js_blogdescription;
                    // dom action
                    jQuery('.mainfixed-menu').addClass('hide');
                    jQuery('.single-page.popup').addClass('active');
                    jQuery('html').addClass('freeze');
                    jQuery('body').addClass('with-popup fixed');
                },
                complete: function () {
                    popupClose();
                    loadnextpost();
                    jQuery('.loading-animate').hide();
                },
                error: function (jqXHR, textStatus) {
                    //alert("Request failed: " + textStatus);
                }
            });
        });
    }

    
    function backhistory(xurl){
            
        var js_postid = getParameterByName('js_pid',xurl);
        console.log('backhistory:'+js_postid);

        if(js_postid){

            jQuery('.loading-animate').show();
            var js_ajax = js_siteurl+'/ajax/?js_pid='+js_postid;
            jQuery.ajax({
                url: js_ajax,
                dataType: 'html',
                method: "GET",
                data: { js_postid: js_postid },
                success: function (result) {
                    // result
                    jQuery('#singleajax').html(result);
                    //dom action
                    jQuery('.mainfixed-menu').toggleClass('hide');
                    jQuery('.single-page.popup').toggleClass('active');
                    jQuery('html').toggleClass('freeze');
                    jQuery('body').addClass('with-popup fixed');
                },
                complete: function () {
                    popupClose();
                    jQuery('.loading-animate').hide();
                },
                error: function (jqXHR, textStatus) {
                    //alert("Request failed: " + textStatus);
                }
            });
        }else{

            jQuery('.single-page.popup').removeClass('active');
            jQuery('.mainfixed-menu').toggleClass('hide');
            jQuery('html').removeClass('freeze');
            jQuery('body').removeClass('with-popup fixed');
            
        }
    }

    /*
    window.onload = function () {
        if (typeof history.pushState === "function") {

            window.onpopstate = function (event) {
                var state  = JSON.stringify(event.state);
                
                if(state!='null'){
                    backhistory(event.target.history.state.url);
                }else{
                    jQuery('.single-page.popup').removeClass('active');
                    jQuery('.mainfixed-menu').toggleClass('hide');
                    jQuery('html').removeClass('freeze');
                    jQuery('body').removeClass('with-popup fixed');

                    history.pushState({url:js_blogname }, js_blogdescription, js_siteurl);
                    document.title = js_blogname + ' - ' + js_blogdescription ;
                    
                }
            };
        }
        else {
            var ignoreHashChange = true;
            window.onhashchange = function () {
                if (!ignoreHashChange) {
                    ignoreHashChange = true;
                    window.location.hash = Math.random();
                    console.log('cek 3');
                    // Detect and redirect change here
                    // Works in older FF and IE9
                    // * it does mess with your hash symbol (anchor?) pound sign
                    // delimiter on the end of the URL
                }else{
                    console.log('cek 4');
                    ignoreHashChange = false;
                }
            };
        }
    }
    */

});

 /* external */
jQuery("a").filter(function(){
    //console.log('run 1');
    return this.hostname && this.hostname !== location.hostname && this.hostname !== 'farm9.staticflickr.com' && this.hostname !== 'www.youtube.com' && this.hostname !== 'www.slideshare.net' && this.hostname !== 'annualreport2016.cifor.org' && this.hostname !== 'i0.wp.com' && this.hostname !== 'i1.wp.com' && this.hostname !== 'i2.wp.com';
}).click(function() {
    //console.log('run 2');
    window.open(this.href);
    return false;
});


// click to scroll
var $root = jQuery('html, body');
jQuery('a[href^="#"]').click(function() {
    //alert('hai bro 2');
    var href = jQuery.attr(this, 'href');
    if (href.length) {
        $root.animate({
            scrollTop: jQuery(href).offset().top
        }, 1000);
    }
    return false;
});

jQuery("#overlay-menu .left-menu a").click(function(e){
    e.preventDefault();
    console.log('jalan cuy');
    var href = jQuery.attr(this, 'href');
    if (href.length) {
        jQuery('html,body').animate({ scrollTop: jQuery(href).offset().top}, 'slow');
    }
    return false;
});


